#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

# import config from current working jesse dir.
import sys
sys.path.append('.')
from config import config
import subprocess

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

def start(update, context):
    """Send a message when the command /start is issued."""
    update.message.reply_text('Hi!')


def help(update, context):
    """Send a message when the command /help is issued."""
    update.message.reply_text('\n'.join(map(str, ['/exchanges - list all workspace exchanges',
        '/import exchange symbol start_date - eg: /import Binance BTCUSDT 2018-06-01',
        '/backtest start_date finish_date - eg: /backtest 2016-01-01 2020-04-06'])))


def echo(update, context):
    """Echo the user message."""
    update.message.reply_text(update.message.text)

def exchanges(update, context):
    names = list(config['exchanges'].keys())
    update.message.reply_text(names)

def import_candles(update, context):
    params = update.message.text.split(' ')
    exchange = params[1]
    symbol = params[2]
    start_date = params[3]
    jesse_import_candles = subprocess.run('echo y | jesse import-candles {} {} {}'.format(exchange, symbol, start_date), shell=True, stdout=subprocess.PIPE)
    update.message.reply_text(str(jesse_import_candles.stdout))

def backtest(update, context):
    params = update.message.text.split(' ')
    start_date = params[1]
    finish_date = params[2]
    jesse_import_candles = subprocess.run('echo y | jesse backtest {} {}'.format(start_date, finish_date), shell=True, stdout=subprocess.PIPE)
    update.message.reply_text(str(jesse_import_candles.stdout))

def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def main():
    """Start the bot."""

    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater(config['telegram']['token'], use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))

    dp.add_handler(CommandHandler("exchanges", exchanges))

    dp.add_handler(CommandHandler("import", import_candles))

    dp.add_handler(CommandHandler("backtest", backtest))

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler(Filters.text, echo))



    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()