import os
from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

with open('requirements.txt') as f:
    required = f.read().splitlines()

setup(name='jesse-telegram',
    version='0.1',
    description='Telegram Bot for Jesse AI.',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='#',
    author='Christian M',
    author_email='chriamue@gmail.com',
    license='MIT',
    install_requires=required,
    packages=['src'],
    entry_points={
        'console_scripts': ['jesse-telegram=src.main:main'],
    },
    zip_safe=False
)
