# Jesse Telegram

Telegram Bot for Jesse workspace.

## quickstart

```bash
git clone https://gitlab.com/chriamue/jesse-telegram.git
cd jesse-telegram
virtualenv venv -p python3
source venv/bin/activate
pip install .
jesse make-project jesse
cd jesse
```

Add token to config.py

```python
config = {

    'telegram': {
        'token': '12...66:AA...EU4'
    }
}
```

```bash
jesse-telegram
```